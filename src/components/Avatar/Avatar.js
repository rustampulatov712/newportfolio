import React from "react";
import {makeStyles} from "@material-ui/core";
import Ava from "../../assets/images/photo_2022-09-07_19-07-35.jpg"

const Avatar = () => {
    const classes = useStyles();
    return (
        <img src={Ava} alt="Rustam Pulatov" className={classes.avatarImg}/>
    );
};

const useStyles = makeStyles((theme) => ({
    avatarImg: {
        borderRadius: '50%',
        width: "270px",
        height: "270px",
        objectFit: 'cover',
        objectPosition: "0 0px",
        boxShadow: theme.shadows[10]
    },
}));

export default Avatar;
