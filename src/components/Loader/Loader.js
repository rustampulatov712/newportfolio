import React from "react";
import LoaderContainer from "../../containers/LoaderContainer";
import LoaderName from "./LoaderName";
const Loader = () => {
    return (
        <LoaderContainer>
            <LoaderName/>
        </LoaderContainer>
    );
};

export default Loader;
