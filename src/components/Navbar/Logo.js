import React, {useContext} from "react";
import {makeStyles, useMediaQuery} from "@material-ui/core";
import {Link} from "react-scroll";
import ThemeContext from "../../contexts/themeContext";

const Logo = ({setHomeIsActive, ...rest}) => {
    const classes = useStyles();
    const { isDarkMode, setIsDarkMode } = useContext(ThemeContext);
    const isMobile = useMediaQuery("(max-width:700px)");
    console.log(isMobile,'ismobile');
    return (
        <Link
            spy
            smooth
            duration={500}
            offset={-70}
            to="home"
            onSetActive={() => setHomeIsActive(true)}
            onSetInactive={() => setHomeIsActive(false)}
            className={classes.root}
        >
            <div className={isDarkMode ? "logoloaderDark ":'logoloader'} style={isMobile ? {left: '21%' } : {}}>
                <span className='logoloaderSpan'>Rustam P.A</span>
            </div>
        </Link>
    );
};

const useStyles = makeStyles((theme) => ({
    root: {
        cursor: "pointer",
    },
}));

export default Logo;

