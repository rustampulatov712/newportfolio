import React, {useContext} from "react";
import { makeStyles } from "@material-ui/core/styles";
import  './../../components/Loader/style.css'

import ThemeContext from "../../contexts/themeContext";
import {useMediaQuery} from "@material-ui/core";

const VideoLogo = (props) => {
    const classes = useStyles();
    const { isDarkMode, setIsDarkMode } = useContext(ThemeContext);
    const isMobile = useMediaQuery("(max-width:700px)");


    return (
        <div className={classes.wrapper} {...props}>
            <div className={isDarkMode ? 'loaderDark':'loader'}>
                <span className={isDarkMode ? 'loaderSpan': 'loaderSpan'} style={isMobile ? {fontSize: '25px'} : {}}>Rustam Pulatov</span>
            </div>
        </div>
    );
};

const useStyles = makeStyles((theme) => ({
    wrapper: {
        overflow:"hidden",
        position: "relative",
        width: "780px",
        height: "90px",
        [theme.breakpoints.down("xs")]: {
            width: "230px",
            height: "53px",
        },
    },
    mask: {
        zIndex: 1,
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        width: "300px",
        [theme.breakpoints.down("xs")]: {
            width: "230px",
            height: "52px",
        },
    },
    video: {
        zIndex: 0,
        position: "absolute",
        top: 0,
        left: "1px",
        right: "1px",
        width: "298px",
        height: "68px",
        [theme.breakpoints.down("xs")]: {
            width: "228px",
            height: "52px",
        },
    },
}));

export default VideoLogo;
