const skillsList = [
    { title: "JavaScript", value: 75 },
    { title: "React JS", value: 75 },
    { title: "TypeScript", value: 80 },
    { title: "Material UI", value: 80 },
    { title: "Mantine UI", value: 80 },
    { title: "Ant Design", value: 60 },
    { title: "GitLab, GitHub", value: 70 },
    { title: "Postman, Swagger", value: 70 },
    { title: "Jira", value: 70 },
];

const projectList = [



    {
        id: 1,
        title: "Project of the state",
        technologies: ["Type Script", "Node JS","React JS", "Cassandra", "Postgres SQL", ],
        backgroundImage:
            "https://cdn.cbeditz.com/cbeditz/preview/black-red-gradient-background-wallpaper-74-11614352798fbqrv1wpuv.jpg",
        frontImage: "oneProject",
    },
    {
        id: 2,
        title: "SetApp Control",
        technologies: ["React JS","Ant Design","Java Spring"],
        backgroundImage:
            "https://img.freepik.com/free-vector/dark-gradient-background-with-copy-space_53876-99548.jpg?size=626&ext=jpg&ga=GA1.2.2102900112.1628985600",
        frontImage: 'setApcontrol',
    },
    {
        id: 3,
        title: "Pos System",
        technologies: ["React JS", "Postgres SQL"],
        backgroundImage:
            "https://media.istockphoto.com/vectors/abstract-purple-vector-background-with-stripes-vector-id972475894?k=6&m=972475894&s=612x612&w=0&h=99AirGMOb64N2-1ZSMYRjEBp2USrAdzXUGzQMh5o6Js=",
        frontImage: 'postSistem',
    },
];

const experienceList = [
    {
        id: 0,
        company: "SETAPP IT Company",
        links: {
            website:"http://setapp.uz/",
            instagram: "https://www.instagram.com/iiv.uz/",
            facebook: "https://www.facebook.com/setappuz/",
            telegram: "https://t.me/SetAppOfficial",

        },
    },
    {
        id: 1,
        company: "SSD",
        links: {
            website: "https://www.stage.ssd.uz//",
            facebook: "https://www.facebook.com/",
            instagram: "https://www.instagram.com/ssd__team/",
            telegram: "https://t.me/SmartSoftDevelopment",
            linkedIn: "https://www.linkedin.com/company/smart-soft-development-llc-ooo-smart-soft-develpment/mycompany/",
        },
    },
    // {
    //     id: 2,
    //     company: "MVD",
    //     links: {
    //         website:"https://iiv.uz/",
    //         instagram: "https://www.instagram.com/iiv.uz/",
    //         facebook: "https://www.facebook.com/iiv.uz",
    //
    //     },
    // },
];

export { skillsList, projectList, experienceList };
